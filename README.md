## OMDB Viewer

This is a simple web page that allows for searching of movies and viewing of
their details, powered by the OMDb API (http://www.omdbapi.com). Movies can
also be "favorited" for later review. An instance of the app is running at
https://still-ocean-36646.herokuapp.com.
