
/*
	This is our main Javascript file. It mostly consists of event listeners,
	and contains the implementation logic for some of the simpler ones. The
	more complex functionality can be found in util.js and templates.js.
*/



// When search field changes, perform search
document.querySelector('#search-input').addEventListener('input', function(event){
	performSearch(this.value);
});



// When one of the view buttons or the search field is clicked, change
// views accordingly
document.querySelector('#search-input').addEventListener('click', switchViewToSearch);
document.querySelector('#view-search').addEventListener('click', switchViewToSearch);
document.querySelector('#view-favorites').addEventListener('click', switchViewToFavorites);



// NOTE: In these two sections we have to attach our click listener to the
// document itself and then check the element that was actually clicked to
// see if it's one that we care about, rather than just attaching the
// listener directly to that one. This is  because our search results and
// favorite buttons get dynamically created when searches are performed.
// If we tried attaching listeners to them when the page first loaded,
// it wouldn't find any and nothing would happen.

// When a search result is clicked, expand and show details
document.addEventListener('click', function(event){
	var movieElement = event.target;

	// Since the listener is attached to the whole document, we need to
	// check if a search result is what was actually clicked
	if(movieElement.classList.contains('result')) {

		// Collapse the result currently open for details, if there is one
		var detailedResult = document.querySelector('.result.detailed');
		if(detailedResult !== null) {
			detailedResult.classList.remove('detailed');
		}

		// Request detailed info from the API
		ajaxRequest({
			url: 'https://www.omdbapi.com/?i=' + event.target.dataset.oid,
			method: 'GET',
			callback: function(response){
				var apiResponse = JSON.parse(response);

				// Construct the necessary HTML and place it in the result
				movieElement.querySelector('.detail-info').innerHTML = constructDetailInfoFromData(apiResponse);

				// Add a CSS class to the search result to expand it
				movieElement.classList.add('detailed');
			}
		});
	}
});

// When a favorite button is clicked, toggle the movie's favorite status
document.addEventListener('click', function(event){
	var favoriteButton = event.target;

	// Since the listener is attached to the whole document, we need to
	// check if a favorite button is what was actually clicked
	if(favoriteButton.classList.contains('favorite-button')) {

		// Check to see if the icon is a filled-in star, to tell whether
		// this movie has already been favorited
		var isFavorited = favoriteButton.querySelector('i').classList.contains('fa-star');

		if(isFavorited) {

			// If it's already been favorited, remove it from favorites
			ajaxRequest({
				url: '/favorites-remove',
				method: 'POST',
				data: JSON.stringify({
					// Get the name and oid from the button's attributes
					name: favoriteButton.dataset.name,
					oid: favoriteButton.dataset.oid
				}),
				callback: function(){

					// Refresh the favorite button appropriately
					favoriteButton.innerHTML = constructFavoriteButton(false);
				}
			});

		} else {

			// If it hasn't been favorited, add it to favorites
			ajaxRequest({
				url: '/favorites',
				method: 'POST',
				data: JSON.stringify({
					// Get the name and oid from the button's attributes
					name: favoriteButton.dataset.name,
					oid: favoriteButton.dataset.oid
				}),
				callback: function(){

					// Refresh the favorite button appropriately
					favoriteButton.innerHTML = constructFavoriteButton(true);
				}
			});
		}

	}
});
