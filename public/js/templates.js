
/*
	When using an MVC framework like AngularJS, this functionality would all be
	handled by the framework. Even though we're not using one here, it's good to
	separate out the templating logic for easy reference, and to place it in
	reusable functions so that we're never duplicating code.
*/


// create the favorite button HTML based on whether or
// not the movie has been favorited
var constructFavoriteButton = function(isFavorited) {
	return '<i class="fa ' + (isFavorited ? 'fa-star' : 'fa-star-o') + '"></i> ' +
			(isFavorited ? 'Favorited' : 'Add to favorites');
};



// create the HTML for one search result, based on the
// API data and whether or not it's been favorited
var constructResultFromData = function(data, isFavorited) {
	return '<div class="result" data-oid="' + data.imdbID + '">' +
				'<div class="favorite-button" data-name="' + data.Title + '" data-oid="' + data.imdbID + '">' +
					constructFavoriteButton(isFavorited) +
				'</div>' +
				'<div class="poster' + ( data.Poster === 'N/A' ? ' no-poster' : '') + '" style="background-image:url(\'' + data.Poster + '\')"></div>' +
				'<div class="info">' +
					'<h1>' + data.Title + '</h1>' +
					'<h3>' + data.Year + '</h3>' +
					'<div class="detail-info"></div>' +
				'</div>' +
			'</div>';
};



// create tht HTML for the detailed info section
// of a search result, based on the API data
var constructDetailInfoFromData = function(data) {
	return '<p>' + data.Genre + '</p>' +
			'<p>Directed by ' + data.Director + '</p>' +
			'<p class="plot">' +
				data.Plot +
			'</p>';
};
