
/*
	The functions in this file mostly exist for readability and to avoid code
	duplication. For example, when assigning functionality to an event listener
	(see site.js), it's much more readable to separate the code that assigns
	the listener from the more complex code that actually implements the
	functionality. And this way, if multiple events ever need to trigger the
	same functionality, it won't need to be copied and pasted (which is almost
	always a bad idea!).
*/



// wrapper for the native ajax API to make it more pleasant
var ajaxRequest = function(parameters) {
	// create request object
	var request = new XMLHttpRequest();

	// give it a function to call when it's done loading
	if(typeof parameters.callback !== 'undefined') {
		request.addEventListener("load", function(){
			parameters.callback(request.response);
		});
	}

	// open the connection
	request.open(parameters.method, parameters.url);

	// send it
	if(typeof parameters.data === 'string') {
		// if caller of the function provided json data, send this along too
		request.setRequestHeader("Content-type", "application/json")
		request.send(parameters.data);
	} else {
		request.send();
	}
};



// perform movie search and populate search results
var performSearch = function(searchString) {
	// we don't want to search without a search string
	if(typeof searchString === 'undefined' || searchString === '') {
		return;
	}

	// first we need to request our favorites list so we can display the search result buttons correctly
	ajaxRequest({
		url: '/favorites',
		method: 'GET',
		callback: function(response){
			var favorites = JSON.parse(response);

			// once we have the favorites, we perform the search with the OMDB API
			ajaxRequest({
				url: 'https://www.omdbapi.com/?s=' + searchString,
				method: 'GET',
				callback: function(response){
					var apiResponse = JSON.parse(response);

					// we'll be referencing the search results container
					// multiple times, so we want to keep a reference on
					// hand rather than finding it every time
					var searchResults = document.querySelector('#search-results');

					// clear out the old search results
					searchResults.innerHTML = '';

					if(apiResponse.Response === 'False') {
						// if no results were found, display a message
						var noResults = '<h1>No Results Found</h1>';
						searchResults.innerHTML += noResults;
					} else {
						// for earch search result...
						for(var i = 0; i < apiResponse.Search.length; i++) {
							// check to see if it's been favorited
							var isFavorited = false;
							for(var j = 0; j < favorites.length; j++) {
								if(apiResponse.Search[i].imdbID === favorites[j].oid) {
									isFavorited = true;
								}
							}

							// construct the HTML string accordingly and add it to the results container
							searchResults.innerHTML += constructResultFromData(apiResponse.Search[i], isFavorited);
						}
					}
				}
			});
		}
	});
};



// request the list of favorite movies and populate them on the page
var loadFavorites = function() {

	// request favorites
	ajaxRequest({
		url: '/favorites',
		method: 'GET',
		callback: function(response){
			var favorites = JSON.parse(response);


			// we'll be referencing the favorites container
			// multiple times, so we want to keep a reference on
			// hand rather than finding it every time
			var favoritesResults = document.querySelector('#favorites-results');

			// clear out the old favorites
			favoritesResults.innerHTML = '';

			// for each favorite...
			for(var i = 0; i < favorites.length; i++) {

				// request its info from the API
				ajaxRequest({
					url: 'https://www.omdbapi.com/?i=' + favorites[i].oid,
					method: 'GET',
					callback: function(response) {
						var apiResponse = JSON.parse(response);

						// construct the HTML string accordingly and add it to the favorites container
						favoritesResults.innerHTML += constructResultFromData(apiResponse, true);;
					}
				});
			}
		}
	});
};



// switch to the search view
var switchViewToSearch = function() {

	// un-hide the search results and hide favorites
	document.querySelector('#search-results').classList.remove('hidden');
	document.querySelector('#favorites-results').classList.add('hidden');

	// toggle active button
	document.querySelector('#view-search').classList.add('active');
	document.querySelector('#view-favorites').classList.remove('active');

	// refresh search results
	performSearch(document.querySelector('#search-input').value);
};



// switch to the favorites view
var switchViewToFavorites = function() {

	// un-hide the favorites and hide search results
	document.querySelector('#search-results').classList.add('hidden');
	document.querySelector('#favorites-results').classList.remove('hidden');

	// toggle active button
	document.querySelector('#view-search').classList.remove('active');
	this.classList.add('active');

	// refresh favorites
	loadFavorites();
};
