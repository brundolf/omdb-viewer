var express = require('express');
var app = express();
var fs = require('fs');
var path = require('path');
var bodyParser = require('body-parser');

app.use(express.static(path.join(__dirname, '/public')));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

app.use('/', express.static(path.join(__dirname, 'public')));

app.get('/favorites', function(req, res){
  var data = fs.readFileSync('./data.json');
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
});

app.post('/favorites', function(req, res){
	console.log(JSON.stringify(req.body))

	if(!req.body.name || !req.body.oid){
	    res.send("Error");
	    return;
	} else {
		var data = JSON.parse(fs.readFileSync('./data.json'));

		var alreadyFavorited = false;
		for(var i = 0; i < data.length; i++) {
			if(data.oid === req.body.oid) {
				alreadyFavorited = true;
			}
		}

		if(!alreadyFavorited) {
			data.push(req.body);
		}

		fs.writeFile('./data.json', JSON.stringify(data));
		res.setHeader('Content-Type', 'application/json');
		res.send(data);
	}
});
app.post('/favorites-remove', function(req, res){

	if(!req.body.name || !req.body.oid){
	    res.send("Error");
	    return;
	} else {
		var data = JSON.parse(fs.readFileSync('./data.json'));

		for(var i = 0; i < data.length; i++) {
			if(data[i].oid === req.body.oid) {
				data.splice(i, 1);
				i--;
			}
		}

		fs.writeFile('./data.json', JSON.stringify(data));
		res.setHeader('Content-Type', 'application/json');
		res.send(data);
	}
});


app.set('port', (process.env.PORT || 8000));
app.listen(app.get('port'), function(){
	console.log("Listening on port 3000");
});
